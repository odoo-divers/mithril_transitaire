{
    'name' : 'Transitaire',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'purchase',
    ],
    'data' : [
    		'transitaire_view.xml',
    ],

    'installable' : True,
    'application' : False,
}
